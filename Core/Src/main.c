/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define ARRAY_QUEUE
#define NOTIFY_RON_TASK 1
#define RON_TASK_ID 0
#define NOTIFY_RILEY_TASK 2
#define RILEY_TASK_ID 1

#define SOUP_DATA 1
#define SOUP_EMPTY 0xFFFE
#define INVALID 0xFFFF

#define SOUPS_SERVED 1000
#define NUMBER_OF_SPACES 16
#define DELAYS 50
#define NUMBER_OF_THREADS 3
#define NUMBER_OF_SOUP_EATERS 2

#define ARRAY_MAX 16
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

osThreadId producerTaskHandle;
osThreadId ronConTaskHandle;
osThreadId rileyConTaskHandle;
osMutexId ladelMutexHandle;
osMutexId uartMutexHandle;
/* USER CODE BEGIN PV */
#ifdef ARRAY_QUEUE
/*
 * This is an circular array queue. The HeadQueue is a pointer to the head of the queue.
 * The tailQueue is the tail of the last queue. The circular array is empty when the tailQueue and
 * the headQueue are equal. The circular array is full when the tailQueue is one element away from the
 * headQueue.
 */
static int arrayQueue[ARRAY_MAX];		//array of the queue
static int* tailQueue = NULL;			//end of the queue
static int* headQueue = NULL;			//start of the queue
static int queueTailIndex = ARRAY_MAX - 1;
static int queueHeadIndex = ARRAY_MAX - 1;
static int queueSize = 0;
#endif
static QueueHandle_t queueRetHandle;
static uint8_t currentSoupEater = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
void StartProducerTask(void const * argument);
void StartRonConTask(void const * argument);
void StartRileyConTask(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

#ifdef ARRAY_QUEUE
  //We start the queue at the end of the queue.
  tailQueue = &arrayQueue[queueTailIndex];
  headQueue = &arrayQueue[queueHeadIndex];
#endif
  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of ladelMutex */
  osMutexDef(ladelMutex);
  ladelMutexHandle = osMutexCreate(osMutex(ladelMutex));

  /* definition and creation of uartMutex */
  osMutexDef(uartMutex);
  uartMutexHandle = osMutexCreate(osMutex(uartMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  queueRetHandle = xQueueCreate(NUMBER_OF_SPACES, sizeof(uint16_t));
  if (queueRetHandle == NULL){ return 0; }
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of producerTask */
  osThreadDef(producerTask, StartProducerTask, osPriorityNormal, 0, 128);
  producerTaskHandle = osThreadCreate(osThread(producerTask), NULL);

  /* definition and creation of ronConTask */
  osThreadDef(ronConTask, StartRonConTask, osPriorityIdle, 0, 128);
  ronConTaskHandle = osThreadCreate(osThread(ronConTask), NULL);

  /* definition and creation of rileyConTask */
  osThreadDef(rileyConTask, StartRileyConTask, osPriorityIdle, 0, 128);
  rileyConTaskHandle = osThreadCreate(osThread(rileyConTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_9;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 71;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RED_LED_Pin|BLUE_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : INPUT_Pin */
  GPIO_InitStruct.Pin = INPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RED_LED_Pin BLUE_LED_Pin */
  GPIO_InitStruct.Pin = RED_LED_Pin|BLUE_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : GREEN_LED_Pin */
  GPIO_InitStruct.Pin = GREEN_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GREEN_LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

#ifdef ARRAY_QUEUE

void EnqueueData(int i)
{
	//if queue full or empty
	if (headQueue == tailQueue)
	{
		//if queue is empty
		if (queueSize == 0)
		{
			//put data into queue and increment size
			arrayQueue[queueTailIndex] = i;
			queueSize++;

			queueTailIndex--;	//set the pointer to the next value

			if (queueTailIndex < 0)
			{
				queueTailIndex = ARRAY_MAX-1;
				tailQueue = &arrayQueue[queueTailIndex];
			}
			else
				tailQueue = &arrayQueue[queueTailIndex];
		}
		else	//must be full
		{
			// label status as full and return
			queueSize = ARRAY_MAX-1;
			return;
		}
	}
	else if (queueSize < ARRAY_MAX-1)	//data in queue but not full
	{
		//put data into queue and increment size
		arrayQueue[queueTailIndex] = i;
		queueSize++;

		//set the pointer to the next value
		queueTailIndex--;
		if (queueTailIndex < 0)
		{
			queueTailIndex = ARRAY_MAX-1;
			tailQueue = &arrayQueue[queueTailIndex];
		}
		else
			tailQueue = &arrayQueue[queueTailIndex];
	}
}

int DequeueData(void)
{
	//if queue full or empty
	if (headQueue == tailQueue)
	{
		//if queue is empty
		if (queueSize <= 0)
		{
			// label status as empty and return
			queueSize = 0;
			return -1;
		}
		else
		{
			int data = arrayQueue[queueHeadIndex];
			arrayQueue[queueHeadIndex] = 0;
			queueSize--;
			return data;
		}
	}
	else
	{
		// get current head and clear it
		int data = arrayQueue[queueHeadIndex];
		arrayQueue[queueHeadIndex] = 0;
		queueSize--;
		if (queueSize < 0)
			queueSize = 0;

		//set the pointer to the next value
		queueHeadIndex--;
		if (queueHeadIndex < 0)
			queueHeadIndex = ARRAY_MAX-1;

		headQueue = &arrayQueue[queueHeadIndex];

		// return data of current head
		return data;
	}
}

#endif

void ServeSoup(int i)
{
#ifdef ARRAY_QUEUE
	xSemaphoreTake(ladelMutexHandle, portMAX_DELAY); //Lock the Mutex
	//Push the integer into the queue
	EnqueueData(i);
	xSemaphoreGive(ladelMutexHandle); //UnLock the Mutex
#else
//	xQueueSendToFront(queueRetHandle, &i, portMAX_DELAY); //puts to the Tail the queue
//	xQueueSendToBack(queueRetHandle, &i, portMAX_DELAY);  //puts to the Head the queue
	xQueueSend(queueRetHandle, &i, portMAX_DELAY);		  //puts to the Head the queue
#endif

	//Notify any waiting threads
	xTaskNotify(ronConTaskHandle, NOTIFY_RON_TASK, eSetBits);
	xTaskNotify(rileyConTaskHandle, NOTIFY_RILEY_TASK, eSetBits);
}

uint16_t TakeSoup(uint8_t id, uint32_t id_notify_bit)
{
	uint16_t bowl = INVALID;
	uint32_t notifValue = 0;

#ifndef ARRAY_QUEUE
	//while soup queue is empty
	while (uxQueueSpacesAvailable(queueRetHandle) == NUMBER_OF_SPACES)
	{
		//wait to be notified
		xTaskNotifyWait(pdFALSE, id_notify_bit, &notifValue, portMAX_DELAY);
		if ((notifValue & id_notify_bit) != 0x00)	//Check if notify value is set
			break;
	}

	if (id == (currentSoupEater % NUMBER_OF_SOUP_EATERS))
	{
		if ((uxQueueSpacesAvailable(queueRetHandle) <= NUMBER_OF_SPACES) &&
				(uxQueueSpacesAvailable(queueRetHandle) > 0))
		{
			xQueueReceive(queueRetHandle, &bowl, portMAX_DELAY);
		}

		currentSoupEater++;
	}
#endif

#ifdef ARRAY_QUEUE
	//while soup queue is empty
	while (queueSize == 0 || queueSize == NUMBER_OF_SPACES)
	{
		//wait to be notified
		xTaskNotifyWait(pdFALSE, id_notify_bit, &notifValue, portMAX_DELAY);
		if ((notifValue & id_notify_bit) != 0x00)	//Check if notify value is set
			break;
	}

	if (id == (currentSoupEater % NUMBER_OF_SOUP_EATERS))
	{
		if ((queueSize <= NUMBER_OF_SPACES) && (queueSize > (int)0))
		{
			xSemaphoreTake(ladelMutexHandle, portMAX_DELAY); //Lock the Mutex
			//get bowl from queue
			bowl = DequeueData();
			//pop from queue
			currentSoupEater++;
			xSemaphoreGive(ladelMutexHandle); //UnLock the Mutex
		}
	}
#endif

	return bowl;
}

void RonTaskFunction(void)
{
	char snum[5];
	uint16_t bowl = 0;
	static uint16_t totalSoupServedRon = 0;

	/* Infinite loop */
	for(;;)
	{
		bowl = TakeSoup(RON_TASK_ID, NOTIFY_RON_TASK);
		if ((bowl != SOUP_EMPTY ) && (bowl != INVALID))
		{
			totalSoupServedRon++;
		}
		else if (bowl == SOUP_EMPTY )
		{
			itoa((int)totalSoupServedRon, snum, 10);
			xSemaphoreTake(uartMutexHandle, portMAX_DELAY); //Lock the Mutex
			//print to the UART the number of soups eaten
			HAL_UART_Transmit(&huart2, (uint8_t*)"Ron ate ", sizeof("Ron ate "), 5);
			HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
			HAL_UART_Transmit(&huart2, (uint8_t*)" soups \n\r", sizeof(" soups \n\r"), 5);
			xSemaphoreGive(uartMutexHandle); //Unlock the mutex
		}
		osDelay(DELAYS/NUMBER_OF_THREADS);
	}
}

void RileyTaskFunction(void)
{
	char snum[5];
	uint16_t bowl = 0;
	static uint16_t totalSoupServedRiley = 0;

  /* Infinite loop */
	for(;;)
	{
		bowl = TakeSoup(RILEY_TASK_ID, NOTIFY_RILEY_TASK);
		if ((bowl != SOUP_EMPTY ) && (bowl != INVALID))
		{
			totalSoupServedRiley++;
		}
		else if (bowl == SOUP_EMPTY )
		{
			itoa((int)totalSoupServedRiley, snum, 10);
			xSemaphoreTake(uartMutexHandle, portMAX_DELAY); //Lock the Mutex
			//print to the UART the number of soups eaten
			HAL_UART_Transmit(&huart2, (uint8_t*)"Riley ate ", sizeof("Riley ate "), 5);
			HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
			HAL_UART_Transmit(&huart2, (uint8_t*)" soups \n\r", sizeof(" soups \n\r"), 5);
			xSemaphoreGive(uartMutexHandle); //Unlock the mutex
		}
		osDelay(DELAYS/NUMBER_OF_THREADS);
	}
}

void StartProducerTaskFunction(void)
{
	/* Infinite loop */
	for(;;)
	  {
		  if (HAL_GPIO_ReadPin(INPUT_GPIO_Port, INPUT_Pin) == GPIO_PIN_SET)
		  {
			  HAL_GPIO_WritePin(RED_LED_GPIO_Port, RED_LED_Pin, GPIO_PIN_SET);
			  for (int i=0; i<SOUPS_SERVED; i++)
			  { // serve the SOUP_BOWLS bowls of soup
				  ServeSoup(SOUP_DATA);
				  osDelay(DELAYS);
			  }
			  ServeSoup(SOUP_EMPTY); // indicate no more soup
			  ServeSoup(SOUP_EMPTY); // indicate no more soup
			  HAL_GPIO_WritePin(RED_LED_GPIO_Port, RED_LED_Pin, GPIO_PIN_RESET);
		  }
		  osDelay(DELAYS/NUMBER_OF_THREADS);
	  }
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartProducerTask */
/**
  * @brief  Function implementing the producerTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartProducerTask */
void StartProducerTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
	StartProducerTaskFunction();
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartRonConTask */
/**
* @brief Function implementing the ronConTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartRonConTask */
void StartRonConTask(void const * argument)
{
  /* USER CODE BEGIN StartRonConTask */
	/* Infinite loop */
	RonTaskFunction();
  /* USER CODE END StartRonConTask */
}

/* USER CODE BEGIN Header_StartRileyConTask */
/**
* @brief Function implementing the rileyConTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartRileyConTask */
void StartRileyConTask(void const * argument)
{
  /* USER CODE BEGIN StartRileyConTask */
  /* Infinite loop */
	RileyTaskFunction();
  /* USER CODE END StartRileyConTask */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
